" This must be first, because it changes other options as side effect.
set nocompatible

syntax enable       " Enable syntax processing/highlighting.

" Spaces and Tabs.
set tabstop=2       " Number of visual space per tab.
set softtabstop=2   " Number of spaces in tab when editing.
set shiftwidth=2    " Number of spaces used by autoindent.
set expandtab       " Tabs are spaces.
set listchars=tab:>.,trail:~    " Show tabs and trailing spaces.
set list            " Show listchars characters.
set autoindent      " Keep same level of indentation when making new line.
set smartindent     " Helps with autoindent.

" UI Config.
set ruler           " Shows line/column numbers in bottom right.
set number          " Show line numbers.
set wildmenu        " Visual autocomplete for command menu.
set lazyredraw      " Redraw screen only when need to.
set showmatch       " Highlighting matching [{()}].
set display+=lastline   " Displays as much of last line that can fit.
set nowrap          " Don't wrap lines.
set linebreak       " Wrap lines based on word, not letter.
set virtualedit=all " Allow cursor to go where there is no character.

" Colours.
set bg=dark         " Tells vim that background colour is dark.

" Searching.
set incsearch       " Search as characters are entered.
set hlsearch        " Highlight matches.
set ignorecase      " Ignore case when searching.
set wrapscan        " Wrap search to start of file.

" Folding.
set foldenable      " Enable folding.
set foldlevelstart=10 " Open most folds by default.
set foldnestmax=10  " 10 nested fold max.
set foldmethod=indent " Fold based on indent level.

" Swap/Backup files.
set swapfile
set dir=~/.tmp      " Where swap files will be stored.

" Filetype specific.
filetype plugin indent on   " Load filetype-specific indent files.

" Allows vim-latex to be loaded for .plaintex files.
let g:tex_flavor='latex'

" Mistyping commands.
command! WQA :wqa
command! WqA :wqa
command! WQa :wqa
command! Wqa :wqa
command! WA :wa
command! Wa :wa
command! WQ :wq
command! Wq :wq
command! W :w
command! Wn :wn
command! WN :wn
command! Wp :wp
command! WP :wp
command! QA :qa
command! Qa :qa
command! Q :q

" Remaps.
let mapleader=","
" Turn off search highlight.
nnoremap <leader><space> :nohlsearch<CR>
" Space open/close folds.
nnoremap <space> za
" Move vertically by visual line.
nnoremap j gj
nnoremap k gk
" Use jk to escape.
  " inoremap jk <ESC>
" Toggle gundo.
  " nnoremap <leader>u :GundoToogle<CR>
" Edit vimrc/zshrc and load vimrc bindings.
nnoremap <leader>ev :vsp ~/.vimrc<CR>
nnoremap <leader>ez :vsp ~/.zshrc<CR>
nnoremap <leader>sv :source ~/.vimrc<CR>
" Save session (with current windows).
nnoremap <leader>s :mksession<CR>
" Open ag.vim.
  " nnoremap <leader>a :Ag
" Yanks to end of line.
nnoremap Y y$
" Remove manual key.
nnoremap K <NOP>
" Add/Remove indentation for selected lines.
vnoremap < <gv
vnoremap > >gv
" Move by displayed line, instead of physical line.
nnoremap j gj
nnoremap k gk
" Shift+Tab will insert an actual tab character.
inoremap <S-Tab> <C-V><Tab>

" Launch.
"call pathogen#infect()

" Auto commands.
" Highlight and wrap around overflow column for latex files.
autocmd BufNewFile,BufRead *.tex setlocal colorcolumn=81 textwidth=81
" Give markdown syntax for .md files.
autocmd BufNewFile,BufFilePre,BufRead *.md setlocal filetype=markdown
" Give groovy syntax for .template files, and use 3 spaces (as much as I don't like it).
autocmd BufNewFile,BufFilePre,BufRead *.template setlocal filetype=groovy tabstop=3 softtabstop=3 shiftwidth=3
" Equally split windows when resizing.
autocmd VimResized * exe "normal! \<c-w>="
" Vim, please take your time to get proper syntax highlighting.
autocmd BufEnter *.template,*.groovy :syntax sync fromstart
" Use tabs for Go files.
autocmd BufNewFile,BufRead *.go setlocal noet ts=4 sw=4 sts=4
" Use bashrc syntax highlighting for bash_aliases file
autocmd BufNewFile,BufRead bash[_-]aliases* call dist#ft#SetFileTypeSH("bash")

if &diff
  colorscheme vimdiff-colors
endif

" Plugins to get.
" vim-fugitive
" vim-git
" vim-better-whitespace
" nerdtree

"" Other settings to consider.
" Maybe automatically correct spellings to british?
  " map <F7> :w<CR>:!ispell -x -d british %<CR><CR>:e<CR><CR>  
" NerdTree stuff.
  " nmap <silent> <c-n> :NERDTreeToggle<CR>
  " let NERDTreeShowBookmarks=1
" Something with getting specific git settings for vim?
  " let git_settings = system("git config --get vim.settings")
  " if strlen(git_settings)
  "     exe "set" git_settings
  " endif
