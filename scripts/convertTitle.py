#!/usr/bin/env python
import os
import re

files = os.popen("ls *.webm *.mkv").read().split("\n")

for f in files[:-1]:
    title, _, remain = f.split(" - ")
    episode, ext = re.compile("\-[A-Z0-9a-z-_]{11}\.").split(remain)

    finalName = "%s - %s.%s" % (episode, title.title(), ext)
    #print("mv '%s' '%s'" % (f, finalName))

    os.system("mv '%s' '%s'" % (f, finalName))

# To download videos:
# youtube-dl --playlist-items 14-20 --yes-playlist https://www.youtube.com/playlist\?list\=PL7atuZxmT954bCkC062rKwXTvJtcqFB8i
