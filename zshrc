# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Name of ZSH theme (in ~/.oh-my-zsh/themes/).
ZSH_THEME="risto"

# Disables command autocorrection.
DISABLE_CORRECTION="true"

# Plugins to load (in ~/.oh-my-zsh/plugins/).
# git: For helpful git functions (current_branch, etc)
plugins=(git git-flow archlinux python)

source $ZSH/oh-my-zsh.sh

local return_code="%(?..%{$fg[red]%}%?%{$reset_color%})"
RPS1="${return_code}"

function git_prompt_info() {
    # Use current_branch to check if we are in git.
    # Instead of weird stuff, check whether current_branch is equal
    # to git_prompt_short_sha. If equal, then we are detached (I think).
    # If not, then we are attached to a branch.

    ref=$(git rev-parse --abbrev-ref HEAD 2> /dev/null) || return

    BRANCH=$(git rev-parse --abbrev-ref HEAD 2> /dev/null | grep -v 'HEAD')
    if [[ -z "$BRANCH" ]]; then
        BRANCH="%{$fg[red]%}Detached"
    else
        BRANCH="%{$fg[green]%}$(current_branch)"
    fi

    STASH=$(git stash list 2> /dev/null | wc -l | grep -v '^0$')
    if [[ -z "$STASH" ]]; then
        STASH=""
    else
        STASH=":%{$fg[blue]%}$STASH%{$reset_color%}"
    fi

    echo "%{$reset_color%}<%{$fg[red]%}$(git_prompt_short_sha)%{$reset_color%}:$BRANCH%{$reset_color%}$STASH>"
}

# Some variables to export.
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:$HOME/.xmonad/bin:$HOME/.bin:/opt/java/bin:$HOME/.cabal/bin:$HOME/myDotfiles/scripts
export TERM='rxvt-256color'
export EDITOR='vim'
export GOROOT='/usr/lib/go'

# Removes some accessibility warnings.
export NO_AT_BRIDGE=1

# enable extended globbing
setopt extended_glob

bindkey "^[[7~" beginning-of-line
bindkey "^[[8~" end-of-line

# Aliases
alias -r ll='ls -l'
alias -r la='ls -la'
alias -r c='clear'
alias -r cls='clear && ls'
alias -r up='cd ..'

alias -r g='/usr/bin/git status'
alias -r gu='/usr/bin/git pull'
alias -r gp='/usr/bin/git push'
alias -r ga='/usr/bin/git add'
alias -r gd='/usr/bin/git diff'
alias -r gb='/usr/bin/git branch'
alias -r gba='/usr/bin/git branch -a'
alias -r gc='/usr/bin/git commit'
alias -r gca='/usr/bin/git commit -a'
alias -r gco='/usr/bin/git checkout'
alias -r gl="/usr/bin/git log --date-order --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"
alias -r gfa='/usr/bin/git fetch --all'

alias -r gm="/usr/bin/git merge --no-ff"
alias -r gff="/usr/bin/git merge --ff-only"

alias -r gdb='gdb -q -ex run'
alias -r vim='vim -O'

alias -r serve='python -m http.server 8080'

alias -r mysql='mysql --sigint-ignore'
alias -r mysqll='mysql --sigint-ignore -u root -p'
alias -r psqll='sudo -u postgres psql'

alias -r ipmasq='sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE'

if [[ -f '/usr/bin/vendor_perl/ack' ]]; then
    alias ack-grep='/usr/bin/vendor_perl/ack'
fi

alias -r ag='ag --pager "less -R"'

alias -r ls="ls --ignore='*.pyc' --color"

alias -r bw="sudo iftop"
alias -r wp="watch -n 1 ping 8.8.8.8"

# Mount but set permissions properly (files: rw- r-- r--, dir: rwxr-xr-x).
alias -r m="sudo mount -o uid=$USER,fmask=0133,dmask=0022"

# Returns the largest directories in home/root (> 1G).
alias -r duh="sudo du -h -d 1 ~ | grep -e '^[0-9.]\+G'"
alias -r dur="sudo du -h -d 1 / | grep -e '^[0-9.]\+G'"

# Returns the top ten directories/files that are using up disk space.
alias -r top10="du -a * | sort -nr | head -10"

alias -r src="source ~/.zshrc"

alias -r sync_time="timedatectl set-ntp true"

# Run keychain for storing private keys.
eval $(keychain --eval --quiet --nogui id_rsa)

alias -r mirage="mirage 2> /dev/null"
